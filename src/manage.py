#!/usr/bin/env python
import os
import sys

# TODO Нужно на всех страницах сделать получение seo-полей в page

if __name__ == "__main__":
    app_path = os.path.abspath(
        os.path.join(os.path.abspath(__file__), os.pardir)
    )

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.configs.dev")

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
