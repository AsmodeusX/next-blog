from settings.configs.base import *

STATIC_ROOT = os.path.abspath(os.path.join(ROOT_DIR, '..', '..', 'static'))

DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'collect_films_master',
        'USER': os.environ.get("DB_USERNAME"),
        'PASSWORD': os.environ.get("DB_PASSWORD"),
        'HOST': '127.0.0.1',
    }
})
