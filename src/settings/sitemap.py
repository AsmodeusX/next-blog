from django.contrib.sitemaps import GenericSitemap
from main.models import MainPageConfig
from about.models import AboutPageConfig
from donate.models import DonatePageConfig
from blog.models import BlogPage, Rubric, Post


main_page = {
    'queryset': MainPageConfig.objects.all(),
    'date_field': 'updated',
}


about_page = {
    'queryset': AboutPageConfig.objects.all(),
    'date_field': 'updated',
}


donate_page = {
    'queryset': DonatePageConfig.objects.all(),
    'date_field': 'updated',
}


blog_page = {
    'queryset': BlogPage.objects.all(),
    'date_field': 'updated',
}


rubric_page = {
    'queryset': Rubric.objects.all(),
    'date_field': 'updated',
}


post_page = {
    'queryset': Post.objects.all(),
    'date_field': 'updated',
}

sitemaps = {
    'main': GenericSitemap(main_page, changefreq='daily', priority=1),
    'about': GenericSitemap(about_page, changefreq='weekly', priority=0.5),
    'donate': GenericSitemap(donate_page, changefreq='weekly', priority=0.5),
    'blog': GenericSitemap(blog_page, changefreq='weekly', priority=0.5),
    'rubric': GenericSitemap(rubric_page, changefreq='weekly', priority=0.5),
    'post': GenericSitemap(post_page, changefreq='weekly', priority=0.5),
}
