from rest_framework import serializers
from blog.models import Post, Rubric, Category, BlogPage
from about.models import AboutPageConfig
from donate.models import DonatePageConfig
from main.models import MainPageConfig


class PostsSerializer(serializers.ModelSerializer):
    preview_url = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = ('id', 'title', 'rubric_slug', 'slug', 'note', 'preview_url', 'get_absolute_url', 'seo')

    def get_preview_url(self, obj):
        request = self.context.get('request')
        preview_url = obj.preview.url

        return request.build_absolute_uri(preview_url)

    @staticmethod
    def get_absolute_url(obj):
        return obj.get_absolute_url

    @staticmethod
    def seo(obj):
        return obj.rubric.seo

    # @staticmethod
    # def categories(obj):
    #     return obj.categories


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ('header', 'description', 'text', 'categories', 'seo')

    @staticmethod
    def seo(obj):
        return obj.seo


class RubricsSerializer(serializers.ModelSerializer):
    preview_url = serializers.SerializerMethodField()

    class Meta:
        model = Rubric
        fields = ('title', 'note', 'preview_url', 'slug', 'get_absolute_url', 'seo')

    def get_preview_url(self, obj):
        request = self.context.get('request')
        if obj.preview:
            preview_url = obj.preview.url
        else:
            preview_url = ''

        return request.build_absolute_uri(preview_url)

    @staticmethod
    def get_absolute_url(obj):
        return obj.get_absolute_url

    @staticmethod
    def seo(obj):
        return obj.seo


class RubricSerializer(serializers.ModelSerializer):

    class Meta:
        model = Rubric
        fields = ('title', 'description', 'text', 'seo')

    @staticmethod
    def seo(obj):
        return obj.seo


class CategoriesSerializer(serializers.ModelSerializer):
    preview_url = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ('title', 'note', 'preview_url', 'slug', 'get_absolute_url', 'seo')

    def get_preview_url(self, obj):
        request = self.context.get('request')
        if obj.preview:
            preview_url = obj.preview.url
        else:
            preview_url = ''

        return request.build_absolute_uri(preview_url)

    @staticmethod
    def get_absolute_url(obj):
        return obj.get_absolute_url

    @staticmethod
    def seo(obj):
        return obj.seo


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('header', 'description', 'text', 'seo')

    @staticmethod
    def seo(obj):
        return obj.seo


class MainSerializer(serializers.ModelSerializer):

    class Meta:
        model = MainPageConfig
        fields = ('header', 'description', 'title', 'text', 'seo')

    @staticmethod
    def seo(obj):
        return obj.seo


class AboutSerializer(serializers.ModelSerializer):

    class Meta:
        model = AboutPageConfig
        fields = ('header', 'description', 'text', 'seo')

    @staticmethod
    def seo(obj):
        return obj.seo


class DonateSerializer(serializers.ModelSerializer):

    class Meta:
        model = DonatePageConfig
        fields = ('header', 'description', 'text', 'widget', 'seo')

    @staticmethod
    def seo(obj):
        return obj.seo


class BlogSerializer(serializers.ModelSerializer):

    class Meta:
        model = BlogPage
        fields = ('header', 'description', 'text', 'seo')

    @staticmethod
    def seo(obj):
        return obj.seo


# class ScriptsSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = Script
#         fields = ('code', )