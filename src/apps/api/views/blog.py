from django.core.exceptions import ObjectDoesNotExist
from blog.models import Post, Rubric, Category, BlogPage


def query_blog():
    config = BlogPage.get_solo()

    return config


def query_posts(slug=None, count=0):
    if slug:
        try:
            rubric = Rubric.objects.get(slug=slug)
            posts = rubric.post_set.reverse()
        except ObjectDoesNotExist:
            return None
    else:
        posts = Post.objects.reverse()
    if count > 0:
        return posts[:count]
    return posts


def query_post(slug):
    if slug:
        try:
            post = Post.objects.get(slug=slug)
        except ObjectDoesNotExist:
            return None
    else:
        return None

    return post


def query_rubric(slug):
    if slug:
        try:
            rubric = Rubric.objects.get(slug=slug)
        except ObjectDoesNotExist:
            return None
    else:
        return []
    return rubric


def query_rubrics(count=0, slug=None):
    rubrics = Rubric.objects.all()

    if slug:
        rubrics = rubrics.filter(category__slug=slug)

    if count > 0:
        rubrics = rubrics[:count]

    return rubrics


def query_categories(count=0):
    categories = Category.objects.all()

    if count > 0:
        return categories[:count]

    return categories


def query_category(slug=None):
    if slug:
        try:
            return Category.objects.get(slug=slug)
            # posts = category.post_set.all()
        except ObjectDoesNotExist:
            return None

    return None

