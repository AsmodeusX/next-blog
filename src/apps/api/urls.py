from django.urls import path
from . import api

app_name = 'api'
urlpatterns = [
  # path('scripts/', api.ScriptsView.as_view(), name='scripts'),
  path('main/', api.MainView.as_view(), name='main'),
  path('about/', api.AboutView.as_view(), name='about'),
  path('donate/', api.DonateView.as_view(), name='donate'),
  path('blog/', api.BlogView.as_view(), name='blog'),
  path('blog/rubric/post/', api.PostView.as_view(), name='blog_rubrics_rubric_post'),
  path('blog/rubric/', api.RubricView.as_view(), name='blog_rubrics_rubric'),
  path('blog/posts/', api.PostsView.as_view(), name='blog_posts'),
  path('blog/rubrics/', api.RubricsView.as_view(), name='blog_rubrics'),
  path('blog/categories/category/', api.CategoryView.as_view(), name='blog_categories_category'),
  path('blog/categories/', api.CategoriesView.as_view(), name='blog_categories'),
]
