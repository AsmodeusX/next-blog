from django.views.generic import View
from django.utils.translation import ugettext_lazy as _

from libs.json_return import json_success, json_bad

from .views.main import query_main
from .views.about import query_about
from .views.donate import query_donate
from .views.blog import query_blog, query_posts, query_post, query_rubrics, query_rubric, query_category, query_categories
# from .views.scripts import query_scripts

from .serializers import (
    PostsSerializer, PostSerializer, RubricsSerializer, RubricSerializer,
    CategoriesSerializer, CategorySerializer, AboutSerializer, BlogSerializer,
    MainSerializer, DonateSerializer#, ScriptsSerializer
)


ERROR_MESSAGES = {
    'unknown_slug': _('Slug is unknown')
}


def format_serialize_data(serializer):
    return {
        'response': serializer.data,
    }


class MainView(View):
    def get(self, request, *args, **kwargs):
        data = query_main()

        serializer = MainSerializer(data, context={
            'request': request,
        }, many=False)

        serialized_data = format_serialize_data(serializer)

        return json_success(serialized_data)


class AboutView(View):
    def get(self, request, *args, **kwargs):
        data = query_about()

        serializer = AboutSerializer(data, context={
            'request': request,
        }, many=False)

        serialized_data = format_serialize_data(serializer)
        return json_success(serialized_data)


class DonateView(View):
    def get(self, request, *args, **kwargs):
        data = query_donate()

        serializer = DonateSerializer(data, context={
            'request': request,
        }, many=False)

        serialized_data = format_serialize_data(serializer)
        return json_success(serialized_data)


class BlogView(View):
    def get(self, request, *args, **kwargs):
        data = query_blog()

        serializer = BlogSerializer(data, context={
            'request': request,
        }, many=False)

        serialized_data = format_serialize_data(serializer)
        return json_success(serialized_data)


class PostsView(View):
    def get(self, request, *args, **kwargs):
        slug = request.GET.get('slug')

        count = request.GET.get('count')

        if count:
            count = int(count)

            data = query_posts(slug, count)
        else:
            data = query_posts(slug)

        serializer = PostsSerializer(data, context={
            'request': request,
        }, many=True)

        serialized_data = format_serialize_data(serializer)

        return json_success(serialized_data)


class PostView(View):
    def get(self, request, *args, **kwargs):
        slug = request.GET.get('slug')
        data = query_post(slug)

        if not data:
            return json_bad({
                'message': ERROR_MESSAGES['unknown_slug']
            })

        serializer = PostSerializer(data, context={
            'request': request,
        }, many=False)

        serialized_data = format_serialize_data(serializer)
        return json_success(serialized_data)


class RubricView(View):
    def get(self, request, *args, **kwargs):
        slug = request.GET.get('slug')

        data = query_rubric(slug)

        serializer = RubricSerializer(data, context={
            'request': request,
        }, many=False)

        serialized_data = format_serialize_data(serializer)

        return json_success(serialized_data)


class RubricsView(View):
    def get(self, request, *args, **kwargs):
        count = request.GET.get('count')
        slug = request.GET.get('slug')

        if count:
            count = int(count)
        else:
            count = 0

        data = query_rubrics(count, slug)

        serializer = RubricsSerializer(data, context={
            'request': request,
        }, many=True)

        serialized_data = format_serialize_data(serializer)

        return json_success(serialized_data)


class CategoriesView(View):
    def get(self, request, *args, **kwargs):
        data = query_categories()
        serializer = CategoriesSerializer(data, context={
            'request': request,
        }, many=True)

        serialized_data = format_serialize_data(serializer)

        return json_success(serialized_data)


class CategoryView(View):
    def get(self, request, *args, **kwargs):
        slug = request.GET.get('slug')
        data = query_category(slug=slug)
        serializer = CategorySerializer(data, context={
            'request': request,
        }, many=False)

        serialized_data = format_serialize_data(serializer)

        return json_success(serialized_data)

#
# class ScriptsView(View):
#     def get(self, request, *args, **kwargs):
#         position = int(request.GET.get('position'))
#         data = query_scripts(position=position)
#         serializer = ScriptsSerializer(data, context={
#             'request': request,
#         }, many=True)
#
#         serialized_data = format_serialize_data(serializer)
#         return json_success(serialized_data)
#
