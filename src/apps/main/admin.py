from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import MainPageConfig
from default_models.admin import SEOPageConfigAdmin


@admin.register(MainPageConfig)
class AboutPageConfigAdmin(SingletonModelAdmin, SEOPageConfigAdmin):
    fieldsets = SEOPageConfigAdmin.fieldsets + (
        (None, {
            'fields': (
                'text',
            ),
        }),
    )

