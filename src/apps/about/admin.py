from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import AboutPageConfig
from default_models.admin import SEOPageConfigAdmin


@admin.register(AboutPageConfig)
class AboutPageConfigAdmin(SingletonModelAdmin, SEOPageConfigAdmin):
    fieldsets = SEOPageConfigAdmin.fieldsets + (
        (None, {
            'fields': (
                'text',
            ),
        }),
    )

