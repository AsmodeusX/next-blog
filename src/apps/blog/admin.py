from django.contrib import admin
from .models import Post, Category, Rubric, BlogPage
from adminsortable2.admin import SortableAdminMixin
from solo.admin import SingletonModelAdmin
from default_models.admin import SEOPageConfigAdmin


@admin.register(BlogPage)
class BlogPageAdmin(SingletonModelAdmin, SEOPageConfigAdmin):
    fieldsets = SEOPageConfigAdmin.fieldsets + (
        (None, {
            'fields': (
                'text',
            ),
        }),
    )


@admin.register(Category)
class CategoryAdmin(SEOPageConfigAdmin):
    fieldsets = SEOPageConfigAdmin.fieldsets + (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'note', 'preview', 'slug', 'text'
            ),
        }),
    )

    prepopulated_fields = {
        'slug': ('title',),
    }
    search_fields = ('title', )
    list_filter = ('title', )
    list_display = ('title', )


@admin.register(Rubric)
class RubricAdmin(SEOPageConfigAdmin):
    fieldsets = SEOPageConfigAdmin.fieldsets + (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'text', 'note', 'preview', 'slug', 'category'
            ),
        }),
    )

    prepopulated_fields = {
        'slug': ('title',),
    }
    # readonly_fields = ('slug', )
    search_fields = ('title', )
    list_filter = ('title', )
    list_display = ('title', 'slug')


@admin.register(Post)
class PostAdmin(SortableAdminMixin, SEOPageConfigAdmin):
    fieldsets = SEOPageConfigAdmin.fieldsets + (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'note', 'preview', 'text', 'rubric', 'categories', 'slug',
            ),
        }),
    )
    search_fields = ('title', )
    list_filter = ('rubric', )
    list_display = ('title', 'rubric', 'sort_order', )
    sortable_by = 'sort_order'
    ordering = ('sort_order', )
    prepopulated_fields = {
        'slug': ('title',),
    }
