# Generated by Django 3.2.6 on 2021-09-23 06:09

import blog.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_category_text'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='preview',
            field=models.ImageField(storage=blog.models.OverwriteStorage(), upload_to=blog.models.upload_to, verbose_name='preview'),
        ),
        migrations.AlterField(
            model_name='rubric',
            name='preview',
            field=models.ImageField(storage=blog.models.OverwriteStorage(), upload_to=blog.models.upload_to, verbose_name='preview'),
        ),
    ]
