from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from solo.models import SingletonModel
from ckeditor.fields import RichTextField
from default_models.models import SEOPageConfig
from autoslug.fields import AutoSlugField
from adminsortable.models import SortableMixin
from default_models.seo import generate_seo
import os
import hashlib


def upload_to(instance, filename):
    ext = os.path.splitext(filename)[1].lower()

    h = hashlib.sha256()

    field = getattr(instance, "preview")

    for chunk in field.chunks():
        h.update(chunk)

    name = h.hexdigest()

    return os.path.join(
        'images/',
        name + ext,
        )


class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


class BlogPage(SEOPageConfig, SingletonModel):
    text = RichTextField(_('text'), blank=True, )

    class Meta:
        verbose_name = _('Blog page')

    def __str__(self):
        return self.title

    @staticmethod
    def get_absolute_url():
        return '/blog/'

    def seo(self):
        return generate_seo(
            seo_title=self.seo_title,
            seo_description=self.seo_description,
            seo_keywords=self.seo_keywords,
            seo_og_title=self.seo_og_title,
            seo_og_description=self.seo_og_description,
            seo_og_site_name=self.seo_og_site_name,
            seo_og_url=self.get_absolute_url(),
            seo_og_image=self.seo_og_image.url or '',
            canonical=self.get_absolute_url()
        )


class Category(SEOPageConfig):
    preview = models.ImageField(_('preview'),
                                storage=OverwriteStorage(),
                                upload_to=upload_to,
                                )
    text = RichTextField(_('text'), blank=True, )
    note = models.CharField(_('note'), blank=True, max_length=255)
    description = RichTextField(_('description'), blank=True, )
    slug = AutoSlugField(_('slug'), populate_from='title', editable=True)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return '/blog/categories/{}'.format(self.slug)

    def seo(self):
        preview = self.preview
        og_image = self.seo_og_image

        image = ''

        if og_image:
            image = og_image.url
        elif preview:
            image = preview.url

        return generate_seo(
            seo_title=self.seo_title,
            seo_description=self.seo_description,
            seo_keywords=self.seo_keywords,
            seo_og_title=self.seo_og_title,
            seo_og_description=self.seo_og_description,
            seo_og_site_name=self.seo_og_site_name,
            seo_og_url=self.get_absolute_url(),
            seo_og_image=image,
            canonical=self.get_absolute_url()
        )


class Rubric(SEOPageConfig):
    preview = models.ImageField(_('preview'),
                                storage=OverwriteStorage(),
                                upload_to=upload_to,
                                )
    category = models.ForeignKey(Category, verbose_name=_('category'), on_delete=models.SET_NULL, null=True, blank=True)
    note = models.CharField(_('note'), blank=True, max_length=255)
    description = RichTextField(_('description'), blank=True, )
    text = RichTextField(_('text'), blank=True, )
    slug = AutoSlugField(_('slug'), populate_from='title', editable=True)

    class Meta:
        verbose_name = _('Rubric')
        verbose_name_plural = _('Rubrics')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return '/blog/{}'.format(self.slug)

    def seo(self):
        preview = self.preview
        og_image = self.seo_og_image

        image = ''

        if og_image:
            image = og_image.url
        elif preview:
            image = preview.url

        return generate_seo(
            seo_title=self.seo_title,
            seo_description=self.seo_description,
            seo_keywords=self.seo_keywords,
            seo_og_title=self.seo_og_title,
            seo_og_description=self.seo_og_description,
            seo_og_site_name=self.seo_og_site_name,
            seo_og_url=self.get_absolute_url(),
            seo_og_image=image,
            canonical=self.get_absolute_url()
        )


class Post(SEOPageConfig, SortableMixin):
    title = models.CharField(_('title'), max_length=64, )
    text = RichTextField(_('text'), blank=True, )
    note = models.CharField(_('note'), blank=True, max_length=255)
    preview = models.ImageField(_('preview'),
                                storage=OverwriteStorage(),
                                upload_to=upload_to)
    rubric = models.ForeignKey(Rubric, verbose_name=_('rubric'), blank=True, null=True, on_delete=models.SET_NULL)
    categories = models.ManyToManyField(Category, verbose_name=_('category'), blank=True)
    slug = AutoSlugField(_('slug'), populate_from='title', editable=True)
    sort_order = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')
        ordering = ['sort_order']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return '/blog/{}/{}'.format(self.rubric.slug, self.slug)

    def seo(self):
        preview = self.preview
        og_image = self.seo_og_image

        image = ''

        if og_image:
            image = og_image.url
        elif preview:
            image = preview.url

        return generate_seo(
            seo_title=self.seo_title,
            seo_description=self.seo_description,
            seo_keywords=self.seo_keywords,
            seo_og_title=self.seo_og_title,
            seo_og_description=self.seo_og_description,
            seo_og_site_name=self.seo_og_site_name,
            seo_og_url=self.get_absolute_url(),
            seo_og_image=image,
            canonical=self.get_absolute_url()
        )

    def rubric_slug(self):
        return self.rubric.slug
