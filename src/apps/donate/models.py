from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel
from default_models.models import SEOPageConfig
from ckeditor.fields import RichTextField
from default_models.seo import generate_seo


class DonatePageConfig(SingletonModel, SEOPageConfig):
    text = RichTextField(_('text'), blank=True, )
    widget = models.TextField(_('iframe widget code'), blank=True)

    class Meta:
        default_permissions = ('change', )
        verbose_name = _('settings')

    def get_absolute_url(self):
        return '/donate/'

    def __str__(self):
        return ugettext('Donate page')

    def seo(self):
        return generate_seo(
            seo_title=self.seo_title,
            seo_description=self.seo_description,
            seo_keywords=self.seo_keywords,
            seo_og_title=self.seo_og_title,
            seo_og_description=self.seo_og_description,
            seo_og_site_name=self.seo_og_site_name,
            seo_og_url=self.get_absolute_url(),
            seo_og_image=self.seo_og_image.url if self.seo_og_image else '',
            canonical=self.get_absolute_url()
        )
