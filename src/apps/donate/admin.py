from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import DonatePageConfig
from default_models.admin import SEOPageConfigAdmin


@admin.register(DonatePageConfig)
class DonatePageConfigAdmin(SingletonModelAdmin, SEOPageConfigAdmin):
    fieldsets = SEOPageConfigAdmin.fieldsets + (
        (None, {
            'fields': (
                'text', 'widget'
            ),
        }),
    )

