from django.conf import settings

def generate_seo(
        seo_title='', seo_description='', seo_keywords='',
        seo_og_title='', seo_og_description='', seo_og_site_name='',
        seo_og_url='', seo_og_image='', canonical=''):
    return {
        'seo_title': seo_title,
        'seo_description': seo_description,
        'seo_keywords': seo_keywords,
        'seo_og_title': seo_og_title,
        'seo_og_description': seo_og_description,
        'seo_og_site_name': seo_og_site_name,
        'seo_og_url': seo_og_url,
        'seo_og_image': seo_og_image,
        'seo_canonical_url': 'https://%s%s' % (settings.DOMAIN.replace('api.', ''), canonical)
    }
