from django.db import models
from django.utils.translation import ugettext_lazy as _


class PageConfig(models.Model):
    title = models.CharField(_('title'), max_length=64, default='')
    header = models.CharField(_('header'), max_length=128, default='')
    description = models.TextField(_('description'), blank=True)

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        abstract = True
        default_permissions = ('change', )
        verbose_name = _('settings')


class SEOPageConfig(PageConfig):
    seo_title = models.TextField(_('title'), blank=True)
    seo_description = models.TextField(_('description'), blank=True)
    seo_keywords = models.TextField(_('keywords'), blank=True)

    seo_og_title = models.TextField(_('title'), blank=True, help_text='Заголовок статьи или страницы')
    seo_og_description = models.TextField(_('description'), blank=True, help_text='Текст краткого описания страницы')
    seo_og_site_name = models.TextField(_('site name'), blank=True, help_text='Название сайта и краткое описание')
    seo_og_image = models.ImageField(_('image'), blank=True, help_text='Картинка при репосте')

    class Meta(PageConfig.Meta):
        abstract = True
