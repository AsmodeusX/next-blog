import PageLayout from "../../layouts/base";
import Text from "../../components/text";
import {fetchJson} from "../../utils/api";
import Block from "../../components/block/block";
import styles from "../../styles/pages/donate/Donate.module.scss"


const Donate = ({...props}) => {
    const page = props.page;
    let text = '';
    if (page.text) {
        text = <Text text={page.text} />;
    }
    const widget_html = <div
        className={styles.DonateIframe}
        dangerouslySetInnerHTML={{__html: page.widget}}>
    </div>;
    if (!!page)
        return (
            <PageLayout props={props}>
                {text}
                <Block content={widget_html} />
            </PageLayout>
        )
    else return <PageLayout />
}

export async function getStaticProps(ctx) {
    const url = `${process.env.API_HOST}/donate/`;
    const response = await fetchJson(url)
        .then(({ response }) => response).catch(reason => {
            console.warn(reason);
        });
    if (response) {
        return {
            props: {
                path: ctx.pathname || null,
                page: response,
                widget: response.widget
            },
            // revalidate: 10, // In seconds
        }
    }
    return {
        props: {

        },
        // revalidate: 10, // In seconds
    }
}

export default Donate;
