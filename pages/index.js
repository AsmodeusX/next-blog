import PageLayout from "../layouts/base";
import Posts from "../components/posts/posts";
import Rubrics from "../components/rubrics/rubrics";
import Categories from "../components/categories/categories";
import {fetchJson} from "../utils/api";
import Text from "../components/text";

const Home = ({...props}) => {
    let text = '';
    const page = props.page;

    if (page && page.text) {
        text = <Text text={page.text} />;
    }
    return (
        <PageLayout props={props}>
            {/*<div className="container">*/}
                {text}
                <Categories count={8} />
                <Rubrics count={8} />
                <Posts count={8} />
            {/*</div>*/}
        </PageLayout>
    )
}


export async function getStaticProps(ctx) {
    const url = `${process.env.API_HOST}/main/`;
    const response = await fetchJson(url)
        .then(({ response }) => response).catch(reason => {
            console.warn(reason);
        });
    if (response) {
        return {
            props: {
                page: response
            },
            // revalidate: 10, // In seconds
        }
    }
    return {
        props: {

        },
        // revalidate: 10, // In seconds
    }
}


export default Home;
