import Layout from "../layouts/base";
import {Grid} from "@material-ui/core";
import Container from "../components/container"


const Custom404 = () => {
    const html = <Grid item xs={12} sm={12} md={12} lg={12}>
        <h1 className={'title title--h1'}>Не правильно, ебаные волки!</h1>
    </Grid>
    return <Layout title={'Страница не найдена'}>
        <Container content={html} />
    </Layout>
};

export default Custom404;


export async function getStaticProps(ctx) {

        return {
            props: {},
            // revalidate: 10, // In seconds
        }
    }
