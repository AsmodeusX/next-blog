import '../styles/globals.scss'
import "../styles/components/menu/menu.scss";
import "../styles/components/logo/logo.scss";
import "../styles/components/header/header.scss";
import "../styles/components/burger/burger.scss";
import "../styles/components/mobile_slide/mobile_slide.scss";
import "../styles/components/block.scss";
import "../styles/components/rubrics/rubrics.scss";
import "../styles/components/categories/categories.scss";
import "../styles/components/post/post.scss";
import "../styles/components/item/item.scss";
import "../styles/components/text.scss";
import '../styles/components/rubrics/rubric.scss';
import "../styles/components/footer/footer.scss";
import "../styles/components/disqus/disqus.scss";

function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default App