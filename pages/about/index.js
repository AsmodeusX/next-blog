import PageLayout from "../../layouts/base";
import Text from "../../components/text";
import {fetchJson} from "../../utils/api";


const About = ({...props}) => {
    const page = props.page;
    if (!!page)
        return (
            <PageLayout props={props}>
                <Text text={page.text} />
            </PageLayout>
        )
    else return <PageLayout />
}

export async function getStaticProps(ctx) {
    let url = `${process.env.API_HOST}/about/`;
    const response = await fetchJson(url)
        .then(({ response }) => response).catch(reason => {
            console.warn(reason);
        });
    if (response) {
        return {
            props: {
                path: ctx.pathname || null,
                page: response
            },
            // revalidate: 10, // In seconds
        }
    }
}

export default About;
