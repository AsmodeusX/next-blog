import PageLayout from "../../../layouts/base";
import Posts from "../../../components/posts/posts";
import {fetchJson} from "../../../utils/api";
import Text from "../../../components/text";

function Rubric ({...props}) {
    const page = props.page;
    let text = '';

    if (page && page.text) {
        text = <Text text={page.text} />;
    }

    return (
        <PageLayout props={props}>
            {text}

            <Posts rubric_slug={props.slug}/>
        </PageLayout>
    )
}
//
// export async function getStaticPaths() {
//     const opts = {};
//     const url = `${process.env.API_HOST}/blog/rubrics/`;
//     const response = await fetchJson(url, opts)
//         .then(({ response }) => response).catch(reason => {
//             console.warn(reason);
//         });
//     let paths = [];
//     if (!!response)
//         response.forEach((el) => {
//             paths.push({
//                 params: {
//                     rubric: el.slug
//                 }
//             })
//         })
//     return {
//         paths: paths,
//         fallback: false,
//     }
// }

//
export async function getServerSideProps(ctx) {
    const slug = ctx.params.rubric,
        opts = {
          slug: slug
        },
        url = `${process.env.API_HOST}/blog/rubric/`;
    const response = await fetchJson(url, opts)
        .then(({ response }) => response).catch(reason => {
            console.warn(reason);
        });
    if (response) {
        return {
            props: {
                path: ctx.pathname || null,
                page: response,
                slug: slug,
            }
        }
    }

    return {
        props: {
            slug: slug,
        }
    }
}

export default Rubric;