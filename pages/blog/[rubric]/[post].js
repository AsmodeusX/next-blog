import PageLayout from "../../../layouts/base";
import Text from "../../../components/text";
import {fetchJson} from "../../../utils/api";
import {Disqus} from "../../../components/disqus/disqus";


const Post = ({...props}) => {
    if (props) {
        const disqus_code = (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = 'https://devblog-ptsuh7snx0.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        });
        const post = props.page;
        if (!!post)
            return (
                <PageLayout props={props}>
                    <Text text={post.text} />
                    <Disqus />
                </PageLayout>
            )
        else return <PageLayout />
    } else  return <PageLayout />
}


export async function getServerSideProps(ctx) {
    const slug = ctx.params.post;
    const opts = {
        slug: slug
    };
    const url = `${process.env.API_HOST}/blog/rubric/post/`;
    const response = await fetchJson(url, opts)
        .then(({ response }) => response).catch(reason => {
            console.warn(reason);
        });

    if (response)
        return {
            props: {
                path: ctx.pathname || null,
                page: response
            }
        }

    return {}
}

export default Post;
