import PageLayout from "../../layouts/base";
import Posts from "../../components/posts/posts";
import Rubrics from "../../components/rubrics/rubrics";
import Categories from "../../components/categories/categories";
import {fetchJson} from "../../utils/api";
import Text from "../../components/text";

const Blog = ({...props}) => {
    const page = props.page;
    let text = '';

    if (page && page.text) {
        text = <Text text={page.text} />;
    }

    return (
        <PageLayout props={props}>
            {text}
            <Categories />
            <Rubrics />
            <Posts />
        </PageLayout>
    )
}

export async function getStaticProps(ctx) {
    let url = `${process.env.API_HOST}/blog/`;
    const response = await fetchJson(url)
        .then(({ response }) => response).catch(reason => {
            console.warn(reason);
        });
    if (response) {
        return {
            props: {
                path: ctx.pathname || null,
                page: response
            },
        }
    } else {
        return {
            props: {

            }
        }
    }
}

export default Blog;