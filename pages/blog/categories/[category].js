import PageLayout from "../../../layouts/base";
import Rubrics from "../../../components/rubrics/rubrics";
import {fetchJson} from "../../../utils/api";
import Text from "../../../components/text";

function Category ({...props}) {
    const page = props.page;
    let text = '';

    if (page && page.text) {
        text = <Text text={page.text} />;
    }

    return (
        <PageLayout props={props}>
            {text}
            <Rubrics category_slug={props.slug}/>
        </PageLayout>
    )
}


export async function getServerSideProps(ctx) {
    const slug = ctx.params.category;
    const opts = {
        slug: slug
    };
    const url = `${process.env.API_HOST}/blog/categories/category/`;
    const response = await fetchJson(url, opts)
        .then(({ response }) => response).catch(reason => {
            console.warn(reason);
        });
    if (response) {
        return {
            props: {
                path: ctx.pathname || null,
                page: response,
                slug: slug,
            }
        }
    }
    return {}
}

export default Category;