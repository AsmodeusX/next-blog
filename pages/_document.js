import Document, {Html, Head, Main, NextScript} from 'next/document';
import {Script} from "../components/scripts/script";
class CustomDocument extends Document {

  render() {
    const yt = "<!-- Yandex.Metrika counter -->\n" +
        "<script type=\"text/javascript\" >\n" +
        "(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};\n" +
        "m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})\n" +
        "(window, document, \"script\", \"https://mc.yandex.ru/metrika/tag.js\", \"ym\");\n" +

        "ym(87967281, \"init\", {\n" +
        "  clickmap:true,\n" +
        "  trackLinks:true,\n" +
        "  accurateTrackBounce:true\n" +
        "});\n" +
        "</script>\n" +
        "  <noscript><div><img src=\"https://mc.yandex.ru/watch/87967281\" style=\"position:absolute; left:-9999px;\" alt=\"\" /></div></noscript>\n" +
        "  <!-- /Yandex.Metrika counter -->"
    return <Html lang="ru">
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Bad+Script&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
        <link rel="icon" href="/favicon.ico" type="image/x-icon" />

        <Script script_code={yt} />
      </Head>
      <body>

      <div className="content">
        <Main/>
      </div>
      <NextScript/>
      </body>
    </Html>;
  }
}

export default CustomDocument;
