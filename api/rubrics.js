import sendApiRequest from "../utils/api";
import {useEffect} from "react";

export function getRubrics () {
    return sendApiRequest(process.env.API_HOST + '/rubrics/');
}
