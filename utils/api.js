export const fetchJson = async (url, opts={}) => {
    const link = new URL(url);
    Object.keys(opts).forEach(key => link.searchParams.append(key, opts[key]));
    const response = (await fetch(link, {
        method: 'GET'
    }));
    return response.json()
};
