import {error} from "next/dist/build/output/log";

const Title = ({title, size = 'h2'}) => {
    let html = '';
    switch (size) {
        case "h1":
            html = <h1 className={'title title--h1'}>{title}</h1>
            break;
        case "h2":
            html = <h2 className={'title title--h2'}>{title}</h2>
            break;
        case "h3":
            html = <h3 className={'title title--h3'}>{title}</h3>
            break;
        case "h4":
            html = <h4 className={'title title--h4'}>{title}</h4>
            break;
        case "h5":
            html = <h5 className={'title title--h5'}>{title}</h5>
            break;
        case "h6":
            html = <h6 className={'title title--h6'}>{title}</h6>
            break;
        default:
            error("Unknown font size")
    }
    return html;
}

export default Title;
