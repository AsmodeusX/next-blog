import {Menu} from "../menu/menu";


export const MobileSlide = () => {

    return (
        <div className={'mobile-slide'}>
            <Menu prefix={'mobile'} />
        </div>
    )
}
