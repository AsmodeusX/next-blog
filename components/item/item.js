import Link from "next/link";
import Image from "next/image";


export default function Item({item}) {
    return <Link href={item.get_absolute_url}><a className={'item'}>
        <span className="item__preview">
            <Image src={item.preview_url} unoptimized={process.env.ENVIRONMENT !== "PRODUCTION"} layout={"fill"} loading={"lazy"} alt={item.title}/>
        </span>
        <span className="information">
            <span className="item__title">{item.title}</span>
            <span className="item__note">{item.note}</span>
        </span>
    </a></Link>
}
