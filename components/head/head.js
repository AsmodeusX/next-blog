import {Grid} from "@material-ui/core";

export function PageHead(page) {
    let html = '';
    let description = '';
    let header = '';

    if (page.page) {

        if (page.page.header)
            header = <h1>{page.page.header}</h1>

        if (page.page && page.page.description)
            description = <div className={'description'} dangerouslySetInnerHTML={{__html: page.page.description}} />

        html = <div className="head-page">
            <div className="container">
                <Grid item xs={12} sm={12} md={6} lg={6}>
                    <div className="head-page__content">
                        {header}
                        {description}
                    </div>
                </Grid>
            </div>
        </div>
    }

    return html;
}
