import Item from "../item/item";

export const Category = ({category}) => {
    return <div className={'category'}>
        <Item item={category}/>
    </div>
}