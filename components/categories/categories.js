import {Category} from "./category";
import Block from "../block/block";
import {useState, useEffect} from "react";
import {Grid} from "@material-ui/core";
import {fetchJson} from "../../utils/api";


const Categories = ({count}=null) => {
    const [data, setData] = useState(null);
    const title = 'Категории нашего сайта';

    const opts = {
        count: count
    }
    useEffect(() => {
        fetchJson(`${process.env.API_HOST}/blog/categories/`, opts)
            .then(({ response }) => setData(response));
    });

    if (data) {
        const html = data.map((category, idx) => {
            return <Grid item xs={12} sm={6} md={4} lg={3} key={'category-' + idx}>
                <div key={idx}>
                    <Category category={category}/>
                </div>
            </Grid>
        })

        return <Block title={title} content={html} />
    }
    return '';
}

export default Categories;
