import Container from "../container";
import BlockTitle from "./title";


const Block = ({title, content}) => {
    let html = <>
        <div className="block__title">
            <BlockTitle title={title}/>
        </div>
        <div className="block__content">
            <Container content={content} />
        </div>
    </>

    return <div className={'block'}>
        {html}
    </div>
}

export default Block;
