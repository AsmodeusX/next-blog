import {Grid} from "@material-ui/core";
import Title from "../title";
import Container from "../container";


const BlockTitle = ({title}) => {
    const html = <Grid item xs={12} sm={12} md={12} lg={12}>
        <Title title={title} size={'h2'}/>
    </Grid>;
    
    return <Container content={html} />
}

export default BlockTitle;
