import {useEffect} from "react";
import Container from "../container";


export function Disqus () {
    let code = '';
    useEffect(() => {
        const disqus_code = (function() { // DON'T EDIT BELOW THIS LINE
            var s = document.createElement('script');
            s.src = 'https://devblog-ptsuh7snx0.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (document.body).appendChild(s);
        });
        disqus_code();
    }, []);
        const html = <div id="disqus_thread"></div>;
        code = <div className={'Disqus block'}>
            <Container content={html} />
        </div>
    return code;
}
