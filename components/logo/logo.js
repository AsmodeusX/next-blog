import {useRouter} from "next/router";
import Link from "next/link";


export function Logo() {
    const defaultTextLogo = "FunnyArticles.online - сайт юмористических рассказов, и не только";
    const mainPath = '/'
    const currentPath = useRouter().pathname

    if (currentPath === mainPath) {
        return <div className={'AltLogo'}>
            {defaultTextLogo}
        </div>
    }

    return (
        <Link href={mainPath}>
            <a className={'AltLogo'}>
                {defaultTextLogo}
            </a>
        </Link>
    )
}
