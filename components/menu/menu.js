import {MenuList} from "./list";

export function Menu(props) {
    return <nav className={'menu menu--' + props.prefix}>
        <MenuList prefix={props.prefix} path={props.path}/>
    </nav>
}
