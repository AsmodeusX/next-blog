import {MenuItem} from "./item";
import {MENU} from "../../constants/menu";


export function MenuList({prefix, path}) {
    const listMenu = MENU.map((item) => {
            const key = `${prefix}-${item.id}`;
            return <MenuItem item={item} key={key} path={path} />
        }
    )
    return <ul className={'menu__list menu-list'}>
        {listMenu}
    </ul>
}
