import Link from "next/link";
import {CloseBurger} from "../burger/burger";


export function MenuItem ({item, path}) {
    let html;
    let classBase = 'menu-list__item';
    const classActive = 'active';

    if (!path || path.path !== item.slug) {
        html = <Link href={item.slug}>
            <a onClick={CloseBurger}>{item.title}</a>
        </Link>
    } else {
        classBase += ' ' + classActive;
        html = <span>{item.title}</span>;
    }

    return <li className={classBase}>{html}</li>;
}
