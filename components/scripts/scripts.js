import {Script} from "./script";
import {useState, useEffect} from "react";
import {fetchJson} from "../../utils/api";


const Scripts = ({position} = null) => {
    const [data, setData] = useState(null);

    let opts = {
        position: position || 0
    }

    useEffect(() => {
        fetchJson(`${process.env.API_HOST}/scripts/`, opts)
            .then(({ response }) => setData(response));
    });

    if (!!data) {
        return data.map((script, idx) => {
            return <Script script_code={script['code']} key={idx} />
        })
    }
    return '';
}

export default Scripts;
