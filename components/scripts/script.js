export const Script = ({script_code}) => {
    return <div className={'Script'} dangerouslySetInnerHTML={{__html: script_code}} />
}