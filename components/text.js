import {Grid} from "@material-ui/core";
import Container from "./container";


const Text = ({text}) => {
    const html = <Grid item xs={12} sm={12} md={8} lg={8}>
        <div className="text-page" dangerouslySetInnerHTML={{__html: text}}>
        </div>
    </Grid>

    return <Container content={html} />
}

export default Text;
