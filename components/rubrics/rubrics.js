import {Rubric} from "./rubric";
import Block from "../block/block";
import {useState, useEffect} from "react";
import {Grid} from "@material-ui/core";
import {fetchJson} from "../../utils/api";


const Rubrics = ({category_slug, count} = null) => {
    const [data, setData] = useState(null);
    const title = 'Рубрики нашего сайта';
    let opts = {
        count: count || 0
    }

    if (!!category_slug) {
        opts['slug'] = category_slug
    }

    useEffect(() => {
        fetchJson(`${process.env.API_HOST}/blog/rubrics/`, opts)
            .then(({ response }) => setData(response));
    });

    if (!!data) {
        const html = data.map((rubric, idx) => {
            return <Grid item xs={12} sm={6} md={4} lg={3} key={'rubric-' + idx}>
                <div key={idx}>
                    <Rubric rubric={rubric}/>
                </div>
            </Grid>
        })

        return <Block title={title} content={html} />
    }
    return '';
}

export default Rubrics;
