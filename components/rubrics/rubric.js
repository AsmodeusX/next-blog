import Item from "../item/item";

export const Rubric = ({rubric}) => {
    return <div className={'rubric'}>
        <Item item={rubric}/>
    </div>
}