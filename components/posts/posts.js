import {Post} from "./post";
import Block from "../block/block";
import {fetchJson} from "../../utils/api";
import {useState, useEffect} from "react";
import {Grid} from "@material-ui/core";
import Container from "../container";


// const fetchJson = async (url, opts={}) => {
//     const link = new URL(url);
//     Object.keys(opts).forEach(key => link.searchParams.append(key, opts[key]));
//     const response = await fetch(link, {
//         method: 'GET'
//     });
//     return response.json();
// };

const Posts = ({rubric_slug, count}=null) => {
    const [data, setData] = useState(null);
    const title = 'Статьи нашего сайта'
    let opts = {
        count: count || 0
    }

    if (!!rubric_slug) {
        opts['slug'] = rubric_slug
    }

    useEffect(() => {
        fetchJson(`${process.env.API_HOST}/blog/posts/`, opts)
            .then(({ response }) => setData(response));
    });
    if (!!data) {
        let html = data.map((post, idx) => {
            return <Grid item xs={12} sm={6} md={4} lg={3}  key={'post-' + idx}>
                <div key={idx}>
                    <Post post={post}/>
                </div>
            </Grid>
        });

        return <Block title={title} content={<Container content={html} />} />
    }
    return '';
}

export default Posts;
