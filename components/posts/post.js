import Item from "../item/item";


export const Post = ({post}) => {
    return <div className={'post'}>
        <Item item={post}/>
    </div>
}
