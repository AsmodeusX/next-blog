import {Logo} from "../logo/logo";
import {Menu} from "../menu/menu";
import Container from "../container";
import {Grid} from "@material-ui/core";
import {useEffect, useState} from "react";


export const Footer = path => {
    const year = new Date().getFullYear();
    const [hostname, setHostname] = useState();

    useEffect(() => {
        setHostname(window.location.hostname)
    }, []);

    const html = <>
        <Grid item xs={12} sm={12} md={5} lg={5}>
            <Logo/>
        </Grid>
        <Grid item xs={12} sm={12} md={2} lg={2}>
            <div className={'domainInfo'}>{hostname}, {year}</div>
        </Grid>
        <Grid item xs={12} sm={12} md={5} lg={5}>
            <Menu prefix='footer' path={path}/>
        </Grid>
    </>

    return (
        <footer>
            <Container content={html} />
        </footer>
    )
}
