import {Grid} from "@material-ui/core";


const Container = ({ content }) => {
    return <div className={'container'}>
        <Grid container justifyContent={'flex-start'} spacing={2} direction={"row"} wrap={"wrap"}>
            {content}
        </Grid>
    </div>
}

export default Container;
