export const Burger = () => {
    function changeMobileSlide() {
        const
            classMenuOpened = 'menu-opened',
            elementHtml = document.getElementsByTagName('html')[0];
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        elementHtml.classList.toggle(classMenuOpened);

    }
    return (
        <div className={'burger'}>
            <button className={"burger__button"} onClick={changeMobileSlide}>
                <span />
                <span />
                <span />
                <span />
                <span />
            </button>
        </div>
    )
};

export const CloseBurger = function () {
    const
        classMenuOpened = 'menu-opened',
        elementHtml = document.getElementsByTagName('html')[0];

    if (elementHtml.classList.contains(classMenuOpened)) {
        elementHtml.classList.remove(classMenuOpened);
    }
};