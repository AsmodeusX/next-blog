import {Logo} from "../logo/logo";
import {Menu} from "../menu/menu";
import Container from "../container";
import {Grid} from "@material-ui/core";
import {Burger} from "../burger/burger";
import NextNprogress from 'nextjs-progressbar';

export const Header = path => {
    const html = <>
        <Grid item xs={6} sm={6} md={6} lg={6}>
            <Logo />
        </Grid>
        <Grid item xs={6} sm={6} md={6} lg={6}>
            <Menu prefix='header' path={path}/>
            <Burger />
        </Grid>
    </>

    return (
        <header>
            <Container content={html} />
            <NextNprogress
                color="#f34723"
                startPosition={0.3}
                stopDelayMs={200}
                height={3}
                showOnShallow={true}
            />
        </header>
    )
}
