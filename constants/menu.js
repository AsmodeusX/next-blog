export const MENU = [
    {
        "id": 1,
        "title": "Статьи",
        "slug": "/blog",
    },
    {
        "id": 2,
        "title": "Об авторе",
        "slug": "/about",
    },
    {
        "id": 3,
        "title": "Поддержать автора",
        "slug": "/donate",
    },
]
