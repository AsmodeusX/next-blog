import {Header} from "../components/header/header";
import {Footer} from "../components/footer/footer";
import Head from "next/head";
import {PageHead} from "../components/head/head";
import {MobileSlide} from "../components/mobile_slide/mobile_slide";


export default function PageLayout({ children, props={}, title=undefined}) {
    let seo_html = '';

    if (props.page) {
        seo_html = <>
            {/* SEO */}
            <title>{props.page.seo.seo_title}</title>
            <meta name="title" content={ props.page.seo.seo_title } />
            <meta name="description" content={ props.page.seo.seo_description } />
            <meta name="keywords" content={ props.page.seo.seo_keywords } />

            {/* OpenGraph */}
            <meta property="og:title" content={ props.page.seo.seo_og_title } />
            <meta property="og:description" content={ props.page.seo.seo_og_description }/>
            <meta property="og:site_name" content={props.page.seo.seo_og_site_name}/>
            <meta property="og:url" content={ props.page.seo.seo_og_url } />
            <meta property="og:image" content={ props.page.seo.seo_og_image }/>
            <link rel="canonical" href={ props.page.seo.seo_canonical_url } />
        </>
    } else if (title) {
        seo_html = <title>{title}</title>
    }

    return (
        <div id={'page'}>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                {seo_html}
            </Head>
            <MobileSlide />
            <Header path={props.path}/>
            <main>
                <PageHead page={props.page} />
                {children}
            </main>
            <Footer path={props.path}/>
        </div>
    )
}
